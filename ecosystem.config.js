module.exports = {
  apps: [
    {
      name: "server",
      script: './src/server/server.js',
      env: {
        "NODE_ENV": "production",
      },
      instances: "max",
      exec_mode: "cluster"
    }
  ]
};
