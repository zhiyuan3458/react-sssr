FROM node:11

ENV NODE_ENV production
WORKDIR /usr/app
COPY package*.json ./
RUN npm install cnpm -g --registry=https://registry.npm.taobao.org
RUN cnpm install
COPY . .
EXPOSE 8080
CMD ["node", "/usr/app/src/server/server.js"]

