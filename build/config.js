module.exports = {
    production: {
        port: 8080,
        publicPath: '/',
        mongodbURL: 'mongodb://47.106.89.51:27017/reactssr'
    },

    development: {
        port: 8000,
        publicPath: '/',
        mongodbURL: 'mongodb://localhost:27017/reactssr'
    }
};
